import md5 from 'md5';
import ResponseDto from '../dto/ResponseDto';
import UserDao from '../dao/UserDao';
import UserRoleEnum from '../enum/UserRoleEnum';

class SignupService {
    
    userDao = new UserDao();
    defaultRoleMask = [ UserRoleEnum.DEFAULT ];

    signup(username = null, mail = null, password = null, displayName = null) {
        return new Promise((resolve) => {
            let responseDto = new ResponseDto();

            if (username === null || mail === null || password  === null || displayName  === null) {
                resolve(responseDto.set(412, 'SIGNUP_MISSING_PARAMETER'));
            } else {
                this.userDao.getByMail(mail).then((result) => {
                    if (result !== null) {
                        throw 'SIGNUP_MAIL_EXIST';
                    }
                    return this.userDao.getByUsername(username);
                }).then((result) => {
                    if (result !== null) {
                        throw 'SIGNUP_USERNAME_EXIST';
                    }
                    return this.userDao.createUser(username, mail, md5(password), this.defaultRoleMask.join(' | '), displayName, true);
                }).then((result) => {
                    if (result === null) {
                        resolve(responseDto.set(500, 'SIGNUP_ERROR'));
                    } else {
                        resolve(responseDto.set(201, 'SIGNUP_SUCCESS'));
                    }
                }).catch((message) => {
                    resolve(responseDto.set(451, message));
                });
            }
        });
    }
}

export default SignupService;