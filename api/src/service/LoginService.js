import md5 from 'md5';
import uuid from 'uuid';
import { isValidMail } from '../helper';
import ResponseDto from '../dto/ResponseDto';
import UserDao from '../dao/UserDao';
import UserRoleEnum from '../enum/UserRoleEnum';
import UserDto from '../dto/UserDto';
import Logger from '../util/Logger';

class SignupService {
    
    logger = Logger.getInstance();
    userDao = new UserDao();
    defaultRoleMask = [ UserRoleEnum.DEFAULT ];

    login(auth = null, password = null) {
        return new Promise((resolve) => {
            let responseDto = new ResponseDto();

            if (auth === null || password === null) {
                resolve(responseDto.set(412, 'LOGIN_MISSING_PARAMETER'));
            } else {
                let getUser = null;
                if (isValidMail(auth)) {
                    getUser = this.userDao.getByMail;
                } else {
                    getUser = this.userDao.getByUsername;
                }

                getUser.call(this.userDao, auth).then((result) => {
                    if (result === null) {
                        resolve(responseDto.set(401, 'LOGIN_WRONG_CREDENTIALS'));
                    } else if (result === false) {
                        resolve(responseDto.set(500, 'LOGIN_ERROR'));
                    } else if (md5(password) !== result['passwordHash']) {
                        resolve(responseDto.set(401, 'LOGIN_WRONG_CREDENTIALS'));
                    } else {
                        let userDto = this.createUserDto(result);
                        return this.userDao.createToken(userDto);
                    }
                }).then((userDto = null) => {
                    if (userDto !== null) {
                        resolve(responseDto.set(200, 'LOGIN_SUCCESS', userDto));
                    }
                }).catch((e) => {
                    this.logger.error(e);
                    resolve(responseDto.set(500, 'LOGIN_ERROR'));
                });
            }
        });
    }

    canAccess(token, userRoleList = null) {
        return this.userDao.getTokenData(token).then((userDto) => {
            if (userDto === null) {
                return false;
            }
            if (userRoleList === null) {
                return true;
            } else {
                let roleMask = UserRoleEnum.get(userDto.role);
                for (let ROLE of userRoleList) {
                    if (roleMask.has(ROLE)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }


    createUserDto(result) {
        let userDto = new UserDto();
        userDto.username = result['username'];
        userDto.mail = result['mail'];
        userDto.token = uuid();
        userDto.role = result['role'];
        userDto.displayName = result['displayName'];
        userDto.avatar = result['avatar'];
        userDto.banner = result['banner'];
        userDto.phone = result['phone'];
        userDto.website = result['website'];
        userDto.lastLogin = result['lastLogin'];
        userDto.active = result['active'];
        userDto.createdAt = result['createdAt'];
        userDto.updatedAt = result['updatedAt'];
        return userDto;
    }
}

export default SignupService;