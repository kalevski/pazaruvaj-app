import MariaDB from '../adapter/MariaDB';
import Redis from '../adapter/Redis';
import Logger from '../util/Logger';

class UserDao {

    logger = Logger.getInstance();
    db = MariaDB.getInstance();
    redis = Redis.getInstance();

    ttl = 60 * 30;


    getByUsername(username) {
        return this.db.user.findOne({
            where: { username }
        }).catch((e) => {
            this.logger.error(this.db.parseError(e));
            return false;
        });
    }

    getByMail(mail) {
        return this.db.user.findOne({
            where: { mail }
        }).catch((e) => {
            this.logger.error(this.db.parseError(e));
            return false;
        });
    }

    getAll(limit, offset) {
        return this.db.user.findAll({
            limit, offset
        }).catch((e) => {
            this.logger.error(this.db.parseError(e));
            return false;
        });
    }

    createUser(username, mail, passwordHash, role, displayName, isActive) {
        return this.db.user.create({
            username: username,
            mail: mail,
            passwordHash: passwordHash,
            role: role,
            displayName: displayName,
            active: isActive
        }).then((result) => {
            return result;
        }).catch((e) => {
            this.logger.error(this.db.parseError(e));
            return null;
        });
    }

    getTokenData(token) {
        return new Promise((resolve) => {
            this.redis.client.get('USERTOKEN:' + token, (value) => {
                if(value !== null) {
                    this.redis.client.expire('USERTOKEN:' + token, this.ttl);
                }
                resolve(JSON.parse(value));
            });
        });
    }

    createToken(userDto) { 
        return new Promise((resolve) => {
            this.redis.client.set('USERTOKEN:' + userDto.token, JSON.stringify(userDto), () => {
                this.redis.client.expire('USERTOKEN:' + userDto.token, this.ttl, () => {
                    resolve(userDto);
                });
            });
        });
    }

}

export default UserDao;