import MariaDB from "../adapter/MariaDB";

import migrationCreateOwner from "./migrationCreateOwner";
import Logger from "../util/Logger";

class Migration {
    
    db = MariaDB.getInstance();
    logger = Logger.getInstance();

    migrationList = [];
    migrationState = [];
    iterator = 0;
    migrated = 0;
    callback = null;
    
    constructor() {
        setTimeout(() => {
            this.migrate();
        }, 1000);
    }

    addMigration(migration) {
        if (typeof migration.name === 'string' && typeof migration.up === 'function') {
            this.migrationList.push(migration);
        } else {
            this.logger.error('Something is wrong with migration file!');
        }
        
    }

    onFinish(callback) {
        this.callback = callback;
    }

    migrate() {
        this.getMigrations().then((result) => {
            this.migrationState = result;
            this.iterator = this.migrationState.length;
            return this.loop();
        }).then(() => {
            if (this.migrated === 0) {
                this.logger.info(`Nothing for migrating!`);
            } else {
                this.logger.info(`Migrated ${this.migrated} scripts up!`);
            }
            this.callback();
        });
    }

    loop() {
        if (this.iterator >= this.migrationList.length) {
            return;
        } else {
            let nextMigration = this.migrationList[this.iterator];
            console.log(nextMigration);
            return nextMigration.up(this.db).then(() => {
                return this.db.migration.create({ name: nextMigration.name });
            }).then(() => {
                this.iterator++;
                this.migrated++;
                return this.loop();
            });
        }
    }

    getMigrations() {
        return this.db.migration.findAll({}).then((result) => {
            return result;
        });
    }

}

export default Migration;