import md5 from 'md5';
import UserDao from "../dao/UserDao";

let name = 'create-owner';

let up = () => {
    let userDao = new UserDao();
    return userDao.createUser('kalevski', 'kalevski@outlook.com', md5('default'), 'SOMETHING', 'App owner', true);
};

export default { name, up };
