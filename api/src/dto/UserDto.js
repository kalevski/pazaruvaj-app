class UserDto {
    username = null;
    mail = null;
    token = null;
    role = null;
    displayName = null;
    avatar = null;
    banner = null;
    phone = null;
    website = null;
    lastLogin = null;
    active = null;
    createdAt = null;
    updatedAt = null;
}

export default UserDto;