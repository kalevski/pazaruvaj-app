import { STRING } from 'sequelize';

export default {
    name: {
        type: STRING,
        allowNull: false,
        unique: true
    }
};