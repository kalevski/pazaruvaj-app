import { STRING, BOOLEAN, DATE } from 'sequelize';

export default {
    username: {
        type: STRING,
        allowNull: false,
        unique: true
    },
    
    mail: {
        type: STRING,
        allowNull: false,
        unique: true
    },
    
    passwordHash: {
        type: STRING,
        allowNull: false
    },
    
    role: {
        type: STRING,
        allowNull: false
    },

    displayName: {
        type: STRING,
        allowNull: true,
        defaultValue: null
    },

    avatar: {
        type: STRING,
        allowNull: true,
        defaultValue: null
    },

    banner: {
        type: STRING,
        allowNull: true,
        defaultValue: null
    },

    facebook: {
        type: STRING,
        allowNull: true,
        unique: true,
        defaultValue: null
    },

    gplus: {
        type: STRING,
        allowNull: true,
        unique: true,
        defaultValue: null
    },

    phone: {
        type: STRING,
        allowNull: true,
        defaultValue: null
    },

    website: {
        type: STRING,
        allowNull: true,
        defaultValue: null
    },

    active: {
        type: BOOLEAN,
        allowNull: false,
        defaultValue: false
    },

    lastLogin: {
        type: DATE,
        allowNull: true,
        defaultValue: null
    }
};