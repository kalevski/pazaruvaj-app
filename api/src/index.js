import Server from './core/Server';

import AuthController from './controller/AuthController';
import MariaDB from './adapter/MariaDB';
import Redis from './adapter/Redis';

MariaDB.getInstance();
Redis.getInstance();

let server = new Server();

server.addController('/auth', AuthController);

server.run(8081);