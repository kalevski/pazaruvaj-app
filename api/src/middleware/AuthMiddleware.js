import LoginService from '../service/LoginService';
import ResponseDto from '../dto/ResponseDto';
import Logger from '../util/Logger';

export default (userRoleList = null) => {    
    return (request, response, next) => {
        let token = request.get('Authorization');
        let loginService = new LoginService();
        let responseDto = new ResponseDto();
        let logger = Logger.getInstance();
        loginService.canAccess(token, userRoleList).then((can) => {
            if (can) {
                next();
            } else {
                response.status(401).json(responseDto.set(401, 'UNAUTHORIZED'));
            }
        }).catch((error) => {
            logger.error('[AUTH MIDDLEWARE]', error);
            response.status(500).json(responseDto.set(500, 'SERVER_ERROR'));
        });
    }
};
