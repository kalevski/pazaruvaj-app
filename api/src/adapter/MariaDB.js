import Sequelize from 'sequelize';
import Logger from '../util/Logger';

import UserModel from '../model/UserModel';
import MigrationModel from '../model/MigrationModel';

class MariaDB {

    logger = Logger.getInstance();
    
    orm = new Sequelize(process.env.MARIADB_URL, {
        dialect: 'mysql',
        operatorsAliases: false,
        pool: {
            max: 5,
            min: 1,
            acquire: 30000,
            idle: 10000
        },
        logging: (message) => {
            this.logger.silly(message);
        }
    });

    user = null;
    migration = null;

    constructor() {
        this.orm.authenticate().then(() => {
            this.logger.debug('Database is ready!');
            
            this.user = this.orm.define('user', UserModel);
            this.migration = this.orm.define('migration', MigrationModel);

            this.user.sync();
            this.migration.sync();

        }).catch(() => {
            this.logger.error('Something wen\'t wrong with database connection! :(');
        });
    }

    parseError(error) {
        let content = '';
        for (let e of error.errors) {
            content += `\n\t${e.type} : ${e.message}`;
        }
        return `${error.name}${content}`
    }
}

var instance = null;
MariaDB.getInstance = function() {
    if (instance === null) {
        instance = new MariaDB();
    }
    return instance;
};

export default MariaDB;
