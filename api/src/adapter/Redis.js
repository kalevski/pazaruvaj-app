import redis from 'redis';
import Logger from '../util/Logger';

class Redis {
    
    client = redis.createClient(process.env.REDIS_URL);
    logger = Logger.getInstance();

    constructor() {
        this.client.on('error', (err) => {
            this.logger.error('Redis error: ' + err);
        });
        
        this.client.on('ready', () => {
            this.logger.debug('Redis client is ready');
        });
    }

    
}

var instance = null;
Redis.getInstance = function() {
    if (instance === null) {
        instance = new Redis();
    }
    return instance;
}

export default Redis;