import Migration from "./migration/Migration";
import migrationCreateOwner from "./migration/migrationCreateOwner";

let migration = new Migration();

migration.onFinish(() => {
    process.exit(0);
});

// list of migrations
migration.addMigration(migrationCreateOwner);
