import Controller from '../core/Controller';
import SignupService from '../service/SignupService';
import LoginService from '../service/LoginService';

class AuthController extends Controller {

    get(request, response) {
        let { auth, password } = request.query;
        let loginService = new LoginService();
        loginService.login(auth, password).then((responseDto) => {
            response.status(responseDto.status).json(responseDto);
        });
    }

    post(request, response) {
        let { username, mail, password, displayName } = request.body;
        let signupService = new SignupService();
        signupService.signup(username, mail, password, displayName).then((responseDto) => {
            response.status(responseDto.status).json(responseDto);
        });
    }
}

export default AuthController;