import AuthService from '../service/AuthService';

let authService = new AuthService();

export default {
    data: authService.getData(),
    authenticated: authService.isAuthenticated(),
    loginErrorCode: null,
    signupErrorCode: null
};