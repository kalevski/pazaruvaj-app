import { AUTH_SIGNUP_SUCCESS, AUTH_SIGNUP_FAILED } from '../../redux/ActionType';
import AuthService from '../../service/AuthService';
import History from '../../helper/History';

export default (displayName, username, mail, password) => {

    let authService = new AuthService();
    let history = History.getInstance();

    return dispatch => {
        authService.signup(displayName, username, mail, password).then((response) => {
            dispatch({type: AUTH_SIGNUP_SUCCESS, payload: {
                code: response.code
            } });
            setTimeout(() => {
                history.forward('/login');
            }, 1500);
        }).catch((response) => {
            console.log(response);
            dispatch({type: AUTH_SIGNUP_FAILED, payload: {
                code: response.code
            }});
        });
    };
}
