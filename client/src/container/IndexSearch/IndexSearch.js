import React, { Component } from 'react';
import SearchComponent from '../../component/SearchComponent';
// import PropTypes from 'prop-types';
import { Grid, Divider, Header, Button } from 'semantic-ui-react';
import './index-search.css';

export default class IndexSearch extends Component {

    render() {
        return (
            <div className="index-search">
                <Grid stackable container>
                    <Grid.Row centered columns={2}>
                        <Grid.Column>
                            <Header textAlign='center' as='h3' dividing>
                                Пронајди, спореди и купи
                            </Header>
                            <SearchComponent />
                        </Grid.Column>
                    </Grid.Row>
                    <Divider horizontal>или</Divider>
                    <Grid.Row centered columns={1}>
                        <Grid.Column>
                            <Header textAlign='center' as='h3'>
                                Можеш да продаваш некој твој продукт
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row centered columns={4} divided>
                        <Grid.Column>
                            <Button fluid color='orange'>Најави се</Button>
                        </Grid.Column>
                        <Grid.Column>
                            <Button fluid secondary>Регистрирај се</Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }

}

// Navigation.propTypes = {
// };