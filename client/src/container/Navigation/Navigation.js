import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu } from 'semantic-ui-react';
import History from '../../helper/History';
import LogoComponent from '../../component/LogoComponent';
import UserNavPanelComponent from '../../component/UserNavPanelComponent';
import SearchComponent from '../../component/SearchComponent';
import GuestNavPanelComponent from '../../component/GuestNavPanelComponent';
import './navigation.css';

export default class Navigation extends Component {

    search = '';
    history = History.getInstance();

    onClick(value) {
        if (value === 'login') {
            this.history.forward('/login');
        } else if (value === 'signup') {
            this.history.forward('/signup');
        } else if (value === 'profile') {
            this.history.forward('/profile');
        } else if (value === 'panel') {
            this.history.forward('/panel');
        } else if (value === 'logout') {
            this.props.logoutAction();
        }
    }

    onLogoClick() {
        this.history.forward('/');
    }

    onSearchChange(value) {
        console.log('search changed', value);
    }

    onSearchSubmit(value) {
        console.log('search submited', value);
    }

    render() {
        return (
            <Menu className="navigation" fluid>
                <Menu.Item>
                    <LogoComponent onClick={() => this.onLogoClick()} />
                </Menu.Item>
                {this.props.search ?
                    <Menu.Item>
                        <SearchComponent
                            defaultValue={'something'}
                            options={this.props.suggestions}
                            onChange={(value) => this.onSearchChange(value)}
                            onSubmit={(value) => this.onSearchSubmit(value)} />
                    </Menu.Item>
                 : ''}
                {
                    this.props.authenticated ?
                    <UserNavPanelComponent displayName={this.props.data.displayName} onClick={(value) => this.onClick(value)} /> :
                    <GuestNavPanelComponent onClick={(value) => this.onClick(value)} />
                }
            </Menu>
        );
    }

}

Navigation.propTypes = {
    search: PropTypes.bool,

    authenticated: PropTypes.bool,
    logoutAction: PropTypes.func,
    data: PropTypes.object,
    searchInput: PropTypes.string
};