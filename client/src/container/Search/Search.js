import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Grid, Dropdown } from 'semantic-ui-react';
import './search.css';
import GridItemComponent from '../../component/GridItemComponent';
const options = [
    { key: 'angular', text: 'Angular', value: 'angular' },
    { key: 'css', text: 'CSS', value: 'css' },
    { key: 'design', text: 'Graphic Design', value: 'design' },
    { key: 'ember', text: 'Ember', value: 'ember' },
    { key: 'html', text: 'HTML', value: 'html' },
    { key: 'ia', text: 'Information Architecture', value: 'ia' },
    { key: 'javascript', text: 'Javascript', value: 'javascript' },
    { key: 'mech', text: 'Mechanical Engineering', value: 'mech' },
    { key: 'meteor', text: 'Meteor', value: 'meteor' },
    { key: 'node', text: 'NodeJS', value: 'node' },
    { key: 'plumbing', text: 'Plumbing', value: 'plumbing' },
    { key: 'python', text: 'Python', value: 'python' },
    { key: 'rails', text: 'Rails', value: 'rails' },
    { key: 'react', text: 'React', value: 'react' },
    { key: 'repair', text: 'Kitchen Repair', value: 'repair' },
    { key: 'ruby', text: 'Ruby', value: 'ruby' },
    { key: 'ui', text: 'UI Design', value: 'ui' },
    { key: 'ux', text: 'User Experience', value: 'ux' },
  ]


export default class Search extends Component {

    render() {
        return (
            <div className="search">
                <Grid stackable container>
                    <Grid.Row>
                        <Grid.Column width={5}>
                            <Dropdown placeholder='Категорија' fluid selection options={options} />
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <Dropdown placeholder='Подкатегорија' fluid selection options={options} />
                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Dropdown placeholder='Продавница' fluid selection options={options} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={4}>
                            <GridItemComponent />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <GridItemComponent />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <GridItemComponent />
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <GridItemComponent />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }

}

// Navigation.propTypes = {
// };