import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';
import SignupFormComponent from '../../component/SignupFormComponent';
import MessageComponent from '../../component/MessageComponent';
import './signup.css';

export default class Signup extends Component {

    submit = (displayName, username, mail, password, checked) => {
        if (checked) {
            this.props.signupAction(displayName, username, mail, password);
        }
    }

    render() {
        return (
            <div className="signup">
                <Grid stackable centered>
                    <Grid.Row>
                        <Grid.Column width={5}>
                            {this.props.signupErrorCode === null ? '' : <MessageComponent message={this.props.signupErrorCode} />}
                            <SignupFormComponent onSubmit={this.submit} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

Signup.propTypes = {
    signupAction: PropTypes.func,
    signupErrorCode: PropTypes.string
};