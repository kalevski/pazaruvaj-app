import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Signup from './Signup';
import signupAction from '../../action/auth/signupAction';

let mapState = (state) => {
    return {
        signupErrorCode: state.auth.signupErrorCode
    };
}

let mapActions = (dispatch) => {
    return {
        signupAction: bindActionCreators(signupAction, dispatch)
    };
}

export default connect(mapState, mapActions)(Signup);
