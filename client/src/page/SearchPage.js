import React from 'react';
import { Container } from 'semantic-ui-react';
import Navigation from '../container/Navigation';
import Search from '../container/Search/Search';

export default () => {
    return (
        <Container fluid>
            <Navigation search />
            <Search />
        </Container>
    );
};
