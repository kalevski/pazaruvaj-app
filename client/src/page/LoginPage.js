import React from 'react';
import { Container } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import Navigation from '../container/Navigation';
import Login from '../container/Login';
import AuthService from '../service/AuthService';

export default () => {
    
    let authService = new AuthService();
    
    return !authService.isAuthenticated() ? (
        <Container fluid>
            <Navigation />
            <Login />
        </Container>
    ) : (
        <Redirect to='/' />
    );
};
