import React from 'react';
import { Container } from 'semantic-ui-react';
import Navigation from '../container/Navigation';
import IndexSearch from '../container/IndexSearch';

export default () => {
    return (
        <Container fluid>
            <Navigation />
            <IndexSearch />
        </Container>
    );
};
