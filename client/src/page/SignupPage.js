import React from 'react';
import { Container } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import Navigation from '../container/Navigation';
import Signup from '../container/Signup';
import AuthService from '../service/AuthService';

export default () => {
    
    let authService = new AuthService();
    
    return !authService.isAuthenticated() ? (
        <Container fluid>
            <Navigation />
            <Signup />
        </Container>
    ) : (
        <Redirect to='/' />
    );
};
