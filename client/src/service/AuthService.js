import md5 from 'md5';
import Api from "../helper/Api";
import Session from '../helper/Session';

class AuthService {
    
    authSession = new Session('auth');
    api = Api.getInstance();

    login(auth, password) {
        return this.api.fetch('GET', `auth?auth=${auth}&password=${password}`).then((response) => {
            debugger;
            this.authSession.save('data', response.data);
            this.authSession.save('hash', this.generateHash(response.data));
            this.authSession.save('authenticated', true);
            return response;
        });
    }

    signup(displayName, username, mail, password) {
        return this.api.fetch('POST', 'auth', {
            data: { displayName, username, mail, password }
        });
    }

    logout() {
        this.authSession.save('data', null);
        this.authSession.save('authenticated', false);
        this.authSession.save('hash', null);
    }

    getData() {
        let data = this.authSession.get('data');
        let currentHash = this.authSession.get('hash');
        let realHash = this.generateHash(data);
        if (currentHash === realHash) {
            return data;
        } else {
            this.authSession.save('authenticated', false);
            this.authSession.save('data', null);
            this.authSession.save('hash', null);
            return null;
        }
    }

    isAuthenticated() {
        return this.authSession.get('authenticated') === null ? false : this.authSession.get('authenticated');
    }

    generateHash(data) {
        return md5(JSON.stringify(data));
    }

}

export default AuthService;