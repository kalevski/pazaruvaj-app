import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';

export default class SearchComponent extends PureComponent {
    
    noResultsMessage = 'Нема предлози';
    placeholder = 'Пребарај...';

    state = {
        value: this.props.value || '',
        options: this.props.options || []
    };

    lastSubmited = null;

    onChange(value) {
        this.setState({
            value: value
        }, () => {
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        });
    }

    onSubmit() {
        if (this.props.onSubmit && this.lastSubmited !== this.state.value) {
            this.props.onSubmit(this.state.value);
            this.lastSubmited = this.state.value;
        }
    }

    render() {
        return (
            <div className="search-component">
                <Dropdown fluid search selection placeholder={this.placeholder}
                    noResultsMessage={this.noResultsMessage} icon='search'
                    searchQuery={this.state.value}
                    options={this.state.options}
                    onBlur={() => this.onSubmit()}
                    onSearchChange={(e) => this.onChange(e.target.value)} />
            </div>
        );
    }
};

SearchComponent.propTypes = {
    value: PropTypes.string,
    options: PropTypes.array,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func
};