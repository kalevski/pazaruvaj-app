import React from 'react';
import { Image } from 'semantic-ui-react';

export default ( { onClick } ) => {
    return (
        <Image onClick={() => onClick()} size='mini' src='/logo.png' />
    );
}