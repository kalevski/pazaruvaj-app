import React from 'react';
import { Button, Menu } from 'semantic-ui-react';

export default ( { onClick } ) => {
    return (
        <Menu.Menu position='right'>
            <Menu.Item>
                <Button onClick={() => onClick('login')} color='orange'>Најави се</Button>
            </Menu.Item>
            <Menu.Item>
                <Button onClick={() => onClick('signup')} color='black'>Регистрирај се</Button>
            </Menu.Item>
        </Menu.Menu>
    );
}