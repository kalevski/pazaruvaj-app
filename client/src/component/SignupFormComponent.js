import React from 'react';
import { Segment, Input, Button, Checkbox } from 'semantic-ui-react';

export default ( { onSubmit } ) => {

    let displayName = null;
    let username = null;
    let mail = null;
    let password = null;
    let agree = false;

    return (
        <Segment>
            <Input onChange={(e, data) => displayName = data.value} fluid type='text' icon='hashtag' iconPosition='left'placeholder='Име и презиме' />
            <br />
            <Input onChange={(e, data) => username = data.value} fluid type='text' icon='user' iconPosition='left' placeholder='Корисничко име' />
            <br />
            <Input onChange={(e, data) => mail = data.value} fluid type='text' icon='at' iconPosition='left' placeholder='Електронска пошта' />
            <br />
            <Input onChange={(e, data) => password = data.value} fluid type='password' icon='key' iconPosition='left' placeholder='Лозинка' />
            <br />
            <Checkbox onChange={(e, data) => agree = data.checked} label='Ги прифаќам правилата и условите' />
            <br />
            <br />
            <Button onClick={() => onSubmit(displayName, username, mail, password, agree)} fluid color='orange'>Регистрирај се</Button>
        </Segment>
    );
};