import React from 'react';
import { Dropdown, Image, Menu } from 'semantic-ui-react';

export default ( { displayName, onClick } ) => {
    
    let options = [
        {
            key: 'user',
            text: (<span>Најавен како <strong>{displayName}</strong></span>),
            disabled: true,
        },
        { key: 'profile', text: 'Профил', value: 'profile' },
        { key: 'panel', text: 'Панел', value: 'panel' },
        { key: 'logout', text: 'Одјави се', value: 'logout' },
    ];

    return (
        <Menu.Menu position='right'>
            <Dropdown value={'user'} onChange={(e, data) => onClick(data.value)} item simple direction='right'
                trigger={
                    <Image src="/user-avatar.png" circular size="mini" />
                } options={options} />
        </Menu.Menu>
    );
}