import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import History from './helper/History';
import reduxStore from './redux/reduxStore';
import { Provider } from 'react-redux';

import IndexPage from './page/IndexPage';
import LoginPage from './page/LoginPage';
import SignupPage from './page/SignupPage';
import SearchPage from './page/SearchPage';
import NotFoundPage from './page/NotFoundPage';

export default class Root extends Component {

    history = History.getInstance();

    render() {
        const store = reduxStore();
        return (
            <Provider store={store}>
                <Router history={this.history.api}>
                    <Switch>
                        <Route exact path="/" component={IndexPage} />
                        <Route exact path="/login" component={LoginPage} />
                        <Route exact path="/signup" component={SignupPage} />
                        <Route exact path="/search" component={SearchPage} />
                        <Route exact component={NotFoundPage} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
};
