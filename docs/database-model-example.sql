CREATE TABLE `product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `duplicate_hash` VARCHAR(63) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `picture_id` INT NOT NULL,
  `description` TEXT NULL,
  `specs` TEXT NOT NULL,
  `price_value` VARCHAR(32) NOT NULL,
  `price_currency` VARCHAR(16) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `hits_no` INT NOT NULL,
  `store_id` INT NOT NULL,
  `category_id` INT NOT NULL,
  `crawling_job_id` INT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `product_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `product_slug_UNIQUE` (`slug` ASC),
  UNIQUE INDEX `product_duplicate_hash_UNIQUE` (`duplicate_hash` ASC));

CREATE TABLE `store` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `picture_id` INT NULL,
  `thumbnail_id` INT NULL,
  `icon_id` INT NULL,
  `products_no` INT NOT NULL,
  `description` TEXT NULL,
  `priority` INT NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(15) NULL,
  `home_page_url` VARCHAR(255) NOT NULL,
  `contact_page_url` VARCHAR(255) NULL,
  `owner_id` INT NOT NULL,
  `category_id` INT NOT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `store_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `store_slug_UNIQUE` (`slug` ASC));

CREATE TABLE `user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password_hash` VARCHAR(255) NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `role` VARCHAR(255) NOT NULL DEFAULT '',
  `display_name` VARCHAR(255) NOT NULL,
  `last_login` TIMESTAMP NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `user_username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `user_mail_UNIQUE` (`mail` ASC));

CREATE TABLE `category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `icon_id` INT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `category_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `category_slug_UNIQUE` (`slug` ASC));

CREATE TABLE `tag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `category_id` INT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `tag_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `tag_slug_UNIQUE` (`slug` ASC));

CREATE TABLE `product_tag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `product_id` INT NOT NULL,
  `tag_id` INT NOT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `product_tag_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `product_tag_product_id_UNIQUE` (`product_id` ASC),
  UNIQUE INDEX `product_tag_slug_UNIQUE` (`tag_id` ASC));

CREATE TABLE `image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `width` VARCHAR(10) NOT NULL,
  `height` VARCHAR(10) NOT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `image_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `image_slug_UNIQUE` (`slug` ASC));

CREATE TABLE `crawling_job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `store_id` INT NOT NULL,
  `priority` INT NOT NULL,
  `average_time` INT NULL,
  `seance_no` INT NOT NULL,
  `items_no` INT NULL,
  `before_start_script` TEXT NULL,
  `extract_domain_script` TEXT NULL,
  `extract_page_script` TEXT NULL,
  `get_urls_script` TEXT NULL,
  `end_process_script` TEXT NULL,
  `settings` TEXT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `crawling_job_id_UNIQUE` (`id` ASC));

CREATE TABLE `crawled_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `duplicate_hash` VARCHAR(63) NOT NULL,
  `seance_no` INT NOT NULL,
  `store_id` INT NOT NULL,
  `crawling_job_id` INT NOT NULL,
  `data` TEXT NOT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `crawled_item_id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `crawled_item_slug_UNIQUE` (`duplicate_hash` ASC));

CREATE TABLE `issue` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `reporter_id` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `screenshot` TEXT NULL,
  `user_agent` TEXT NULL,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `issue_id_UNIQUE` (`id` ASC));

-- Add admin
INSERT INTO `user` (`username`, `password_hash`, `mail`, `role`, `display_name`, `active`)
VALUES('kalevski', 'd048c69ab134112532763b626e640bd8', 'kalevski@outlook.com', 'ADMIN', 'Daniel Kalevski', 1);
