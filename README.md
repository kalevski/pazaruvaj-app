# Pazaruvaj App

# Before first run
`docker run -d --net="host" -e MYSQL_USER=pazaruvaj-user -e MYSQL_PASSWORD=pazaruvaj -e MYSQL_ROOT_PASSWORD=pazaruvaj -e MYSQL_DATABASE=pazaruvaj --name p-mariadb-dev mariadb:latest`

`docker run -d --net="host" --name p-redis-dev redis:alpine`

`npm install` - execute in api & client

# Run
`docker run -d p-mariadb-dev`

`docker run -d p-redis-dev`

`npm start` - execute in api

`npm start` - execute in clinet

# Stop
`docker stop p-mariadb-dev`

`docker stop p-redis-dev`

# Production
- Change passwords
- `docker-compose up`